<?php

$asciiArray = range(utf8_encode(chr(44)),utf8_encode(chr(124)));
shuffle($asciiArray);
$randomKey = array_rand($asciiArray, 1);
$missingChar = $asciiArray[$randomKey];
unset($asciiArray[$randomKey]);

echo $missingChar;
?>