<?php
class StaticMembers {
   static public function weekDays(): array
   {
       return array(
            0=>'Monday',
            1=>'Tuesday',
            2=>'Wednesday',
            3=>'Thursday',
            4=>'Friday',
            5=>'Saturday',
            6=>'Sunday',
        );
   }
}
?>