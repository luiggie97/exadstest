<?php
include 'Database.php';
include 'StaticMembers.php';
$database = new Database;

$menuOption=fopen("php://stdin","r");
echo("WELCOME TO THE TV SERIES TEST!!!".PHP_EOL);
echo("1) Check series by current Date".PHP_EOL);
echo("2) Check series by typed Date".PHP_EOL);
echo("3) Filter By Tv Series Title".PHP_EOL);
$menuChoice = stream_get_contents($menuOption, 1);

switch($menuChoice){
    case 1: checkSeries($database);
        break;
    case 2:              
        $typedDate = (string)readline('Type a date (Y-m-d)'.PHP_EOL);
        $typedHour = (string)readline('Type an hour (h:i:sa)'.PHP_EOL);        
        checkSeries($database, $typedDate, $typedHour);
        break;
    case 3:
        $typedName = (string)readline('Type a series title to filter'.PHP_EOL);    
        filterSeries($database, $typedName);
        break;
    default: echo "Choose a valid option!";
        break;
}

/**
 * Function to call the where clause and filter by typed name
 * @param Database $database
 * @param string $typedName
 * @return void
 */
function filterSeries(Database $database, string $typedName): void
{
    $value = [
        'value' => $typedName,
        'and' => false,
    ];
    $result = $database->selectWhere('test','tv_series', 'title', $value);

    if($result)
        echo "TV Series is ".$result['title'].
        " and will air on ".$result['week_day'].
        " at ". $result['show_time'].
        " on ". $result['channel'];
    else 
        echo 'There is no TV Series with this title';
}

/**
 * Function to prepare the date and print the result
 * @param Database $database
 * @param string $typedDate
 * @param string $hour
 * @return void
 */
function checkSeries(Database $database, string $typedDate = "Y-m-d" , string $hour = "h:i:sa"): void
{  
    $date =  date($typedDate); 
    $unixTimestamp = strtotime($date);
    $dayOfWeek = date("l", $unixTimestamp);

    $key = array_search($dayOfWeek, StaticMembers::weekDays());
   
    $result = getResultFromWhere($database, $key, $hour);
    
    while(!$result){       
        $key++;
        $result = getResultFromWhere($database, $key, $hour, false);
    }
    
    echo "The next TV Series is ".$result['title'].
    " and will air on ".StaticMembers::weekDays()[$key].
    " at ". $result['show_time'].
    " on ". $result['channel'];
}

/**
 * Function to prepare all the data to filter on where clause
 * @param Database $database
 * @param integer $key
 * @param string $hour
 * @param boolean $and
 */
function getResultFromWhere(Database $database, int $key, string $hour, bool $and = true)
{
    $value = [
        'value' =>StaticMembers::weekDays()[$key],
        'hour' => date($hour),       
        'and' => $and,      
    ];
    return $database->selectWhere('test','tv_series', 'week_day', $value);
}
?>
