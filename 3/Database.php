<?php

include 'Config.php';


class Database extends Config { 
    public $db;
    function __construct() 
    {   
        $this->db = $this->connect();  
    }

    /**
     * Create a database connection
     * @return mysqli
     */
    function connect(): mysqli  {       
      return new mysqli($this->servername, $this->username, $this->password);
    }

    /**
     * Close database connection
     * @return void
     */
    function disconnect() {
      $this->db
           ->close();
    }

    /**
     * select all dataset from a table
     * @param string $database
     * @param string $table
     * @param string $tableJoin
     * @return array
     */
    function selectAll(string $database, string $table, string $tableJoin = null): array{
        $query = $tableJoin ? 'SELECT * FROM '.$database.'.'.$table.' a INNER JOIN '.$database.'.'.$tableJoin.' b ON a.id = b.id_tv_series'
                : 'SELECT * FROM '.$database.'.'.$table;

        return $this->db
              ->query($query)
              ->fetch_all();
    }
    function selectWhere(string $database, string $table, string $column ,array $value)  {
        $query = "SELECT * FROM ".$database.".".$table." a INNER JOIN ".$database.".tv_series_intervals
         b ON a.id = b.id_tv_series WHERE ".$column." = '".$value['value']."'";
        
         if($value['and'])
            $query .=" AND show_time > '".$value['hour']."'";
       
         return $this->db
                ->query($query)
                ->fetch_assoc();
    }
}
?>