CREATE DATABASE IF NOT EXISTS test;

USE test;

CREATE TABLE tv_series (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'pk_tv_series_id',
    title VARCHAR(100), 
    channel VARCHAR(30),
    gender VARCHAR(30),
    CONSTRAINT pk_tv_series_id PRIMARY KEY (id)
);

CREATE TABLE tv_series_intervals (
    id INT NOT NULL AUTO_INCREMENT COMMENT 'pk_tv_series_intervals_id',
    id_tv_series INT,
    week_day VARCHAR(30),
    show_time TIME,
    CONSTRAINT pk_tv_series_intervals_id PRIMARY KEY (id),
    FOREIGN KEY (id_tv_series) REFERENCES tv_series(id)
);

INSERT INTO tv_series (title, channel, gender)
VALUES ('Nightmare in Elm Street', 'CNN', 'Horror');

INSERT INTO tv_series (title, channel, gender)
VALUES ('Dragon Ball Z', 'NHK', 'Anime');

INSERT INTO tv_series (title, channel, gender)
VALUES ('The Simpsons', 'FOX', 'Cartoon');

INSERT INTO tv_series (title, channel, gender)
VALUES ('The Avengers', 'SIC', 'Adventure');

INSERT INTO tv_series (title, channel, gender)
VALUES ('The Fast & Furious', 'TVE', 'Action');

INSERT INTO tv_series_intervals (id_tv_series, week_day, show_time)
VALUES (1, 'Sunday','03:00:00');

INSERT INTO tv_series_intervals (id_tv_series, week_day, show_time)
VALUES (2, 'Monday', '06:15:00');

INSERT INTO tv_series_intervals (id_tv_series, week_day, show_time)
VALUES (3, 'Tuesday', '09:30:00');

INSERT INTO tv_series_intervals (id_tv_series, week_day, show_time)
VALUES (4, 'Wednesday','12:45:00');

INSERT INTO tv_series_intervals (id_tv_series, week_day, show_time)
VALUES (5, 'Friday', '15:00:00');