<?php 

$limit = 100;

for ($i=1; $i <= $limit; $i++) { 
    $stringToPrint = $i;
    $countMultiples = 0;
    for ($n=1; $n <= $limit; $n++) { 
        if(isMultiple($i,$n) ){
            $stringToPrint .='{'.$n.'},';        
            $countMultiples++;
        }
    }
    if($countMultiples==1)
        $stringToPrint = $i.' [PRIME]';
    $stringToPrint = rtrim($stringToPrint,',').PHP_EOL;
    echo $stringToPrint;
}

/**
 * Function to check if the number is Multiple
 * @param integer $input
 * @param integer $numberToCheck
 * @return boolean
 */
function isMultiple(int $input,int $numberToCheck): bool{
    return $numberToCheck % $input == 0; 
}
?>