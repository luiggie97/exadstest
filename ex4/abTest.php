<?php

namespace ex4;

use Exads\ABTestData;

class abTest
{
    /**
     * Function to get the promotion by Id and return all the information about it
     * @param int $promoId
     * @return array
     */
    public function getData(int $promoId): array
    {
        $abTest = new ABTestData($promoId);

        $promotion = $abTest->getPromotionName();
        $designs = $abTest->getAllDesigns();

        return [
            'abteste'=> $abTest,
            'promotion'=> $promotion,
            'designs'=> $designs,
        ];
    }

    /**
     * Function to get a random design based on splitPercent
     * @param array $designs
     * @return array
     */
    public function getRandomDesign(array $designs):array
    {
        $randomChoice = rand(1,100);
        $checkSum = 0;

        foreach ($designs as $key=>$item) {
            $checkSum += $item['splitPercent'];
            if ($checkSum >= $randomChoice )
                return $item;
        }
    }
}