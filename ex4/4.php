<?php

namespace ex4;
require_once realpath("vendor/autoload.php");
include 'abTest.php';

$abTest = new abTest();

$menuOption=fopen("php://stdin","r");
echo("WELCOME TO THE AB TEST!!!".PHP_EOL);
echo("1) Choose the promotion 1".PHP_EOL);
echo("2) Choose the promotion 2".PHP_EOL);
echo("3) Choose the promotion 3".PHP_EOL);
$menuChoice = stream_get_contents($menuOption, 1);

if(checkMenuOptionChoice($menuChoice)) {
    $arrayPromotion = $abTest->getData($menuChoice);
    $randomDesign = $abTest->getRandomDesign($arrayPromotion['designs']);
    echo("The design name is ".$randomDesign['designName'].PHP_EOL);
    echo("The design ID is ".$randomDesign['designId'].PHP_EOL);
    echo("The design percent is ".$randomDesign['splitPercent'].PHP_EOL);
}    
else
    echo("Choose a valid option!");

/**
 * Function to validate the menu option
 * @param int $menuChoice
 * @return bool
 */
function checkMenuOptionChoice(int $menuChoice): bool
{
    if($menuChoice === 1 || $menuChoice === 2 || $menuChoice === 3)
        return true;
    
    return false;
}

?>